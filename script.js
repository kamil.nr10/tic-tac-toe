const player1 = 'fa-circle-o';
const player2 = 'fa-times';
let round = 1;
const board = [
    ['', '', ''],
    ['', '', ''],
    ['', '', '']
];
const combinatios = [
    [0, 1, 2],
    [2, 4, 5],
    [6, 7, 8],
    [0, 3, 4],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
]

// funkcja querySelectorAll zwraca obiekt tablicopodobny za pomocą operator SPREAD [...] zamieniamy ją na tablice i moemy zastosować metodę forEach

const boxes = [...document.querySelectorAll('.box')];
boxes.forEach(box => box.addEventListener('click', pick));


function pick(event) {
    const {
        row,
        column
    } = event.target.dataset;
    const turn = round % 2 === 0 ? player2 : player1;
    if (board[row][column] !== '') return;
    event.target.classList.add(turn);
    board[row][column] = turn;
    round++;
    console.log(check());

};


function check() {
    const result = board.reduce((total, row) => total.concat(row));
    let winner = null;
    let moves = {
        'fa-times': [],
        'fa-circle-o': []
    };
    console.log(moves);
    result.forEach((field, index) => moves[field] ? moves[field].push(index) : null);
    combinatios.forEach(combination => {
        if (combination.every(index => moves[player1].indexOf(index) > -1)) {
            winner = 'Wygrał gracz 1';
        }
        if (combination.every(index => moves[player2].indexOf(index) > -1)) {
            winner = 'Wygrał gracz 2';
        }

    })
    return winner;
}